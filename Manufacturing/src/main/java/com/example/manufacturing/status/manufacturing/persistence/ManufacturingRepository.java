package com.example.manufacturing.status.manufacturing.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManufacturingRepository extends JpaRepository<Manufacturing,Integer> {

}
