package com.example.manufacturing.status.sold.persistence;


import com.example.manufacturing.base.config.RunConfig;
import com.example.manufacturing.products.productscount.persistence.ProductsCount;
import com.example.manufacturing.user.producers.persistence.Producers;
import com.example.manufacturing.user.warehouser.persistence.Warehouser;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "sold", schema = RunConfig.db)
@Data
@EntityListeners(AuditingEntityListener.class)
public class Sold {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @CreatedDate
    @Column(name = "sold_time")
    private LocalDateTime soldTime;

    @OneToMany(mappedBy = "sold", fetch = FetchType.LAZY)
    private Collection<ProductsCount> products;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warehouser_id" , referencedColumnName = "id")
    private Warehouser warehouser;
}
