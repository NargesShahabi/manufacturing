package com.example.manufacturing.status.manufacturing.presentation;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ManufacturingModel {

    private Integer id;
    private Date startTime;
    private String producerName;
    private Integer producerCode;
    private Integer producerId;
    private List<ManufacturingProductsModel> productsType;

}
