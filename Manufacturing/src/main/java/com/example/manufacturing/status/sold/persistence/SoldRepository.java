package com.example.manufacturing.status.sold.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SoldRepository extends JpaRepository<Sold,Integer> {
    Optional<Sold> findById(Integer integer);
}
