package com.example.manufacturing.status.manufactured.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManufacturedRepository extends JpaRepository<Manufactured,Integer> {
}
