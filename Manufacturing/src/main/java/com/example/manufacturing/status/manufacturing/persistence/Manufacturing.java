package com.example.manufacturing.status.manufacturing.persistence;


import com.example.manufacturing.base.config.RunConfig;
import com.example.manufacturing.products.productscount.persistence.ProductsCount;
import com.example.manufacturing.user.producers.persistence.Producers;
import lombok.Data;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Data
@Table(name = "manufacturing", schema = RunConfig.db)
@EntityListeners(AuditingEntityListener.class)
public class Manufacturing {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @CreatedDate
    @UpdateTimestamp
    @Column(name = "start_time")
    private Date startTime;

    @OneToMany(mappedBy = "manufacturingLine", fetch = FetchType.LAZY)
    private Collection <ProductsCount> productList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "producer_id" , referencedColumnName = "id")
    private Producers producer;
}
