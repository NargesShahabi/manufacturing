package com.example.manufacturing.status.manufacturing.service;

import com.example.manufacturing.status.manufacturing.presentation.ManufacturingModel;

import java.util.List;

public interface ManufacturingService {

    List<ManufacturingModel> index();

    ManufacturingModel create(ManufacturingModel manufacturingModel);

    ManufacturingModel update(ManufacturingModel manufacturingModel);

    String delete(Integer id);

    ManufacturingModel show(Integer id);
}
