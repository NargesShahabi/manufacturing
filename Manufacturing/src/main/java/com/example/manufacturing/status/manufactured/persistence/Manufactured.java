package com.example.manufacturing.status.manufactured.persistence;

import com.example.manufacturing.base.config.RunConfig;
import com.example.manufacturing.products.productscount.persistence.ProductsCount;
import com.example.manufacturing.user.producers.persistence.Producers;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;

@Entity
@Table(name = "manufactured", schema = RunConfig.db)
@Data
@EntityListeners(AuditingEntityListener.class)
public class Manufactured {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @CreatedDate
    @Column(name = "manufactured_time",nullable = false)
    private LocalDateTime manufacturedTime;

    @OneToMany(mappedBy = "manufactured", fetch = FetchType.LAZY)
    private Collection<ProductsCount> productList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "producer_id" , referencedColumnName = "id")
    private Producers producer;
}
