package com.example.manufacturing.status.sold.presentation;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
public class SoldModel {

    private Integer id;
    private LocalDateTime soldTime;
    private String  warehouserName;
    private Integer warehouserCode;
    private Integer warehouserId;
    private List<Integer> productsCountIds;
}
