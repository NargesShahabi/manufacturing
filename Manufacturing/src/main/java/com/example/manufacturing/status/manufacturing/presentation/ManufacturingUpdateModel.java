package com.example.manufacturing.status.manufacturing.presentation;

import com.example.manufacturing.products.productscount.presentation.ProductsCountModel;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ManufacturingUpdateModel {

    Integer manufacturingId;
    Integer producerCode;
    List<ProductsCountModel> forUpdate = new ArrayList<>();
    List<ProductsCountModel> forCreate = new ArrayList<>();
    List<Integer> forDelete = new ArrayList<>();
}
