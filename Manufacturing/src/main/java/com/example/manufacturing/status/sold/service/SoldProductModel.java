package com.example.manufacturing.status.sold.service;

import lombok.Data;

@Data
public class SoldProductModel {
    private Integer productId;
    private String productName;
    private Integer number;
}
