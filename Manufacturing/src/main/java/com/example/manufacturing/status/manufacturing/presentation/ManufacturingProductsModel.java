package com.example.manufacturing.status.manufacturing.presentation;

import lombok.Data;

@Data
public class ManufacturingProductsModel {

    private Integer productId;
    private String productName;
    private Integer number;

}
