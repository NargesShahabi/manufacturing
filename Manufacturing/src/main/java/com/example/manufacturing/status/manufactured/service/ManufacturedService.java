package com.example.manufacturing.status.manufactured.service;

import com.example.manufacturing.status.manufactured.presentation.ManufacturedModel;

import java.util.List;

public interface ManufacturedService {

    List<ManufacturedModel> index();

    ManufacturedModel create(ManufacturedModel manufacturedModel);

    String delete(Integer id);

    ManufacturedModel show(Integer id);
}
