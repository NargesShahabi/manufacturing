package com.example.manufacturing.status.sold.presentation;

import com.example.manufacturing.status.sold.service.SoldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/sold")
public class SoldController {


    private SoldService soldService;

    @Autowired
    public SoldController(SoldService soldService) {
        this.soldService = soldService;
    }

    @GetMapping({"/", ""})
    public List<SoldModel> index() {
        return soldService.index();
    }


    @PostMapping({"/", ""})
    public SoldModel create(@RequestBody SoldModel soldModel) {
        return soldService.create(soldModel);
    }

    @PutMapping({"/", ""})
    public SoldModel update(@RequestBody SoldModel soldModel) {
        return soldService.update(soldModel);
    }

    @DeleteMapping({"/{id}"})
    public String delete(@PathVariable Integer id){
        return soldService.delete(id);
    }

    @GetMapping({"/{id}"})
    public SoldModel show(@PathVariable Integer id){
        return soldService.show(id);
    }
}
