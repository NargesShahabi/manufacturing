package com.example.manufacturing.status.manufactured.service;

import com.example.manufacturing.base.exceptions.EntityNotFoundException;
import com.example.manufacturing.base.exceptions.NotAllowedException;
import com.example.manufacturing.products.allproducts.presentation.AllProductsModel;
import com.example.manufacturing.products.allproducts.service.AllProductsServiceImpl;
import com.example.manufacturing.products.productioninfo.persistence.ProductsInfoRepository;
import com.example.manufacturing.products.productscount.persistence.ProductsCount;
import com.example.manufacturing.products.productscount.persistence.ProductsCountRepository;
import com.example.manufacturing.products.productscount.presentation.ProductsCountModel;
import com.example.manufacturing.products.productscount.service.ProductsCountServiceImpl;
import com.example.manufacturing.status.manufactured.persistence.Manufactured;
import com.example.manufacturing.status.manufactured.persistence.ManufacturedRepository;
import com.example.manufacturing.status.manufactured.presentation.ManufacturedModel;
import com.example.manufacturing.user.producers.persistence.Producers;
import com.example.manufacturing.user.producers.persistence.ProducersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service

@Qualifier("ManufacturedServiceImpl")
@Transactional(rollbackFor = Exception.class)
public class ManufacturedServiceImpl implements ManufacturedService {

    private ManufacturedRepository manufacturedRepository;
    private ProducersRepository producersRepository;
    private ProductsCountRepository productsCountRepository;
    private ProductsCountServiceImpl productsCountService;
    private AllProductsServiceImpl allProductsService;
    private ProductsInfoRepository productsInfoRepository;


    @Autowired
    public ManufacturedServiceImpl(ManufacturedRepository manufacturedRepository,
                                   ProducersRepository producersRepository,
                                   ProductsCountServiceImpl productsCountService,
                                   ProductsCountRepository productsCountRepository,
                                   AllProductsServiceImpl allProductsService,
                                   ProductsInfoRepository productsInfoRepository) {
        this.manufacturedRepository = manufacturedRepository;
        this.producersRepository = producersRepository;
        this.productsCountService = productsCountService;
        this.productsCountRepository = productsCountRepository;
        this.allProductsService = allProductsService;
        this.productsInfoRepository = productsInfoRepository;
    }

    @Override
    public List<ManufacturedModel> index() {
        return manufacturedRepository.findAll().stream().map(value -> this.convertToModel(value)).collect(Collectors.toList());
    }

    @Override
    public ManufacturedModel create(ManufacturedModel manufacturedModel) {
        //Producer validation
        Producers producer = (Producers) producersRepository.findByProducerCode(manufacturedModel.getProducerCode())
                .orElseThrow(() -> new EntityNotFoundException("Producer with code "+ manufacturedModel.getProducerCode() +" not found"));
        manufacturedModel.setProducerId(producer.getId());

        //Products id validation
        ArrayList<Integer> wrongIds = (ArrayList<Integer>) manufacturedModel.getProductsCountIds()
                .stream().filter(value-> !productsCountRepository.existsById(value)).collect(Collectors.toList());
        if(!wrongIds.isEmpty())
            throw new EntityNotFoundException("ProductsCount with ids "+ wrongIds +" not found");

        //Products creation admit
        ArrayList<Integer> invalidIds = (ArrayList<Integer>) manufacturedModel.getProductsCountIds()
                .stream().filter(value-> productsCountRepository.findById(value).get().getExisted()!=null)
                .collect(Collectors.toList());
        if(!invalidIds.isEmpty())
            throw new NotAllowedException("ProductsCount with ids "+ invalidIds +" can not be confirmed");

        Manufactured manufactured = manufacturedRepository.save(convertToEntity(manufacturedModel));
        //AllProducts creation + ProductsCount update
        List<ProductsCountModel> productsCountList = new ArrayList<>();
        for(Integer items:manufacturedModel.getProductsCountIds()){
            List<AllProductsModel> allProductsModel = new ArrayList<>();
            ProductsCount PC = productsCountRepository.getById(items);
            for(int i=1; i<=PC.getNumber(); i++){
                allProductsModel.add(new AllProductsModel(null, (items*10 + i),items , null, null, null));
            }
            PC.setExisted(true);
            PC.setManufactured(manufactured);
            ProductsCountModel PCModel = ProductsCountServiceImpl.convertToModel(PC);
            PCModel.setManufacturedId(manufactured.getId());
            PCModel.setExpirationDate(manufactured.getManufacturedTime()
                    .plusMonths(productsInfoRepository.getById(PC.getProductInfo().getId()).getShelfLife()));
            PCModel.setAllProductsModels(allProductsService.create(allProductsModel));
            productsCountList.add(PCModel);
        }
        productsCountService.update(productsCountList);


        manufacturedModel.setId(manufactured.getId());
        manufacturedModel.setManufacturedTime(manufactured.getManufacturedTime());
        manufacturedModel.setProducerName(producer.getFName() +" "+producer.getLName());
        return manufacturedModel;
    }

    @Override
    public String delete(Integer id) {
        try{
            manufacturedRepository.deleteById(id);
        }catch (Exception ex){
            ex.getMessage();
        }

        if(!manufacturedRepository.getById(id).getProductList().isEmpty())
            throw new NotAllowedException("Delete is not allowed");

        return "Successfully deleted";
    }

    @Override
    public ManufacturedModel show(Integer id) {
        Manufactured manufactured =  manufacturedRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Manufactured with id " + id  + " not found"));
        return convertToModel(manufactured);
    }


    public static ManufacturedModel convertToModel(Manufactured manufactured){
        ManufacturedModel manufacturedModel = new ManufacturedModel();
        manufacturedModel.setId(manufactured.getId());
        manufacturedModel.setManufacturedTime(manufactured.getManufacturedTime());
        manufacturedModel.setProducerName(manufactured.getProducer().getFName() + " "+ manufactured.getProducer().getLName());
        manufacturedModel.setProducerCode(manufactured.getProducer().getProducerCode());
        manufacturedModel.setProducerId(manufactured.getProducer().getId());
        if (manufactured.getProductList()!=null){
            manufactured.getProductList().stream().map(ManufacturedServiceImpl::convertToManufacturedProductModel)
                    .collect(Collectors.toList());
        }
        return manufacturedModel;
    }

    private static ManufacturedProductModel convertToManufacturedProductModel(ProductsCount productsCount) {
        ManufacturedProductModel MPM = new ManufacturedProductModel();
        MPM.setProductId(productsCount.getId());
        MPM.setProductName(productsCount.getProductInfo().getName());
        MPM.setNumber(productsCount.getNumber());
        return MPM;
    }

    public static Manufactured convertToEntity(ManufacturedModel manufacturedModel){
        Manufactured manufactured = new Manufactured();
        Producers producer = new Producers();
        producer.setId(manufacturedModel.getProducerId());
        manufactured.setProducer(producer);
        manufactured.setId(manufacturedModel.getId());
        manufactured.setProductList(manufacturedModel.getProductsCountIds()
                                    .stream()
                                    .map((Integer t) -> new ProductsCount(t))
                                    .collect(Collectors.toList()));
        return manufactured;
    }

}
