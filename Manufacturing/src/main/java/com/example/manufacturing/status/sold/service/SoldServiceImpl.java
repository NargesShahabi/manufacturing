package com.example.manufacturing.status.sold.service;

import com.example.manufacturing.base.exceptions.EntityNotFoundException;
import com.example.manufacturing.base.exceptions.NotAllowedException;
import com.example.manufacturing.products.productscount.persistence.ProductsCount;
import com.example.manufacturing.products.productscount.persistence.ProductsCountRepository;
import com.example.manufacturing.products.productscount.presentation.ProductsCountModel;
import com.example.manufacturing.products.productscount.service.ProductsCountService;
import com.example.manufacturing.products.productscount.service.ProductsCountServiceImpl;
import com.example.manufacturing.status.sold.persistence.Sold;
import com.example.manufacturing.status.sold.persistence.SoldRepository;
import com.example.manufacturing.status.sold.presentation.SoldModel;
import com.example.manufacturing.user.warehouser.persistence.Warehouser;
import com.example.manufacturing.user.warehouser.persistence.WarehouserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@Qualifier("SoldServiceImpl")
@Transactional(rollbackFor = Exception.class)
public class SoldServiceImpl implements SoldService {

    private SoldRepository soldRepository;
    private WarehouserRepository warehouserRepository;
    private ProductsCountRepository productsCountRepository;
    private ProductsCountService productsCountService;
    private ProductsCountServiceImpl productsCountServiceImpl;

    @Autowired
    public SoldServiceImpl(SoldRepository soldRepository,
                           WarehouserRepository warehouserRepository,
                           ProductsCountRepository productsCountRepository,
                           ProductsCountService productsCountService,
                           ProductsCountServiceImpl productsCountServiceImpl) {
        this.soldRepository = soldRepository;
        this.warehouserRepository = warehouserRepository;
        this.productsCountRepository = productsCountRepository;
        this.productsCountService = productsCountService;
        this.productsCountServiceImpl = productsCountServiceImpl;
    }

    @Override
    public List<SoldModel> index() {
        return soldRepository.findAll().stream()
                .map(value -> this.convertToModel(value)).collect(Collectors.toList());
    }

    @Override
    public SoldModel create(SoldModel soldModel) {

        //Warehouser validation
        Warehouser warehouser = (Warehouser) warehouserRepository.findByWarehouserCode(soldModel.getWarehouserCode())
                .orElseThrow(() -> new EntityNotFoundException("Warehouser with code "+ soldModel.getWarehouserCode() +" not found"));
        soldModel.setWarehouserId(warehouser.getId());

        //ProductsCount id validation
        ArrayList<Integer> wrongIds = new ArrayList<>();
        ArrayList<Integer> invalidIds = new ArrayList<>();
        for(Integer id: soldModel.getProductsCountIds()){
            Optional<ProductsCount> PC =  productsCountRepository.findById(id);
            if(PC.isEmpty())
                wrongIds.add(id);
            else if (PC.get().getExisted()==null || !PC.get().getExisted())
                invalidIds.add(id);
        }
        if(!wrongIds.isEmpty())
            throw new EntityNotFoundException("ProductsCount with ids "+ wrongIds +" not found");

        if(!invalidIds.isEmpty())
            throw new NotAllowedException("ProductsCount with ids "+ invalidIds +" can not be unsold");

        Sold sold = soldRepository.save(convertToEntity(soldModel));

        //ProductsCount update
        List<ProductsCountModel> productsCountList = new ArrayList<>();
        for(Integer items:soldModel.getProductsCountIds()){
            ProductsCount PC = productsCountRepository.getById(items);
            ProductsCountModel PCModel = ProductsCountServiceImpl.convertToModel(PC);
            PCModel.setSoldId(sold.getId());
            PCModel.setExisted(false);
            productsCountList.add(PCModel);
        }
        productsCountService.update(productsCountList);

        //Convert To Model
        soldModel.setId(sold.getId());
        soldModel.setSoldTime(sold.getSoldTime());
        soldModel.setWarehouserName(warehouser.getFName() +" "+warehouser.getLName());
        return soldModel;
    }

    @Override
    public SoldModel update(SoldModel soldModel) {

        //Sold id validation
        Sold sold = soldRepository.findById(soldModel.getId())
                .orElseThrow(() -> new EntityNotFoundException("Sold with id "+ soldModel.getId()+" not found"));

        //Warehouser validation
        Warehouser warehouser = (Warehouser) warehouserRepository.findByWarehouserCode(soldModel.getWarehouserCode())
                .orElseThrow(() -> new EntityNotFoundException("Warehouser with code "+ soldModel.getWarehouserCode() +" not found"));
        soldModel.setWarehouserId(warehouser.getId());

        //ProductsCount id validation
        ArrayList<Integer> wrongIds = new ArrayList<>();
        ArrayList<Integer> invalidIds = new ArrayList<>();
        for(Integer id: soldModel.getProductsCountIds()){
           Optional<ProductsCount> PC =  productsCountRepository.findById(id);
           if(PC.isEmpty())
               wrongIds.add(id);
           else if (PC.get().getSold()==null || !PC.get().getSold().getId().equals(soldModel.getId())
                   ||PC.get().getExisted()==null || PC.get().getExisted())
               invalidIds.add(id);
        }
        if(!wrongIds.isEmpty())
            throw new EntityNotFoundException("ProductsCount with ids "+ wrongIds +" not found");

        if(!invalidIds.isEmpty())
            throw new NotAllowedException("ProductsCount with ids "+ invalidIds +" can not be unsold");

        //ProductsCount and sold updates
        List<ProductsCount> productsCount = (List<ProductsCount>) sold.getProducts();

        List<Integer> newPCIds = new ArrayList<>();
        int num = 0;
        for(ProductsCount pc : productsCount){
            Boolean check = false;
            for(Integer pId : soldModel.getProductsCountIds()){
                if(pc.getId().equals(pId)) {
                    pc.setSold(null);
                    pc.setExisted(true);
                    check = true;
                    num++;
                }
            }
            if (!check)
                newPCIds.add(pc.getId());
        }
        if(num == productsCount.size())
            soldRepository.deleteById(soldModel.getId());
        else {
            soldModel.setProductsCountIds(newPCIds);
            soldRepository.save(convertToEntity(soldModel));
        }
        productsCountService.update(productsCount.stream()
                .map(value-> productsCountServiceImpl.convertToModel(value))
                .collect(Collectors.toList()));

        //Convert To Model
        soldModel.setId(sold.getId());
        soldModel.setSoldTime(sold.getSoldTime());
        soldModel.setWarehouserName(warehouser.getFName() +" "+warehouser.getLName());
        return soldModel;
    }

    @Override
    public String delete(Integer id) {
        try{
            soldRepository.deleteById(id);
        }catch (Exception ex){
            ex.getMessage();
        }

        if(!soldRepository.getById(id).getProducts().isEmpty())
            throw new NotAllowedException("Delete is not allowed");

        return "Successfully deleted";
    }

    @Override
    public SoldModel show(Integer id) {
        Sold sold =  soldRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Sold with id " + id  + " not found"));
        return convertToModel(sold);
    }


    private SoldModel convertToModel(Sold sold){
        SoldModel soldModel = new SoldModel();
        soldModel.setId(sold.getId());
        soldModel.setSoldTime(sold.getSoldTime());
        soldModel.setWarehouserName(sold.getWarehouser().getFName() + " "+ sold.getWarehouser().getLName());
        soldModel.setWarehouserCode(sold.getWarehouser().getWarehouserCode());
        soldModel.setWarehouserId(sold.getWarehouser().getId());
        if (sold.getProducts()!=null){
             sold.getProducts().stream().map(SoldServiceImpl::convertSoldProductModel)
                    .collect(Collectors.toList());
        }

        return soldModel;
    }

    private static SoldProductModel convertSoldProductModel(ProductsCount productsCount) {
        SoldProductModel SPM = new SoldProductModel();
        SPM.setProductId(productsCount.getId());
        SPM.setProductName(productsCount.getProductInfo().getName());
        SPM.setNumber(productsCount.getNumber());
        return SPM;
    }

    private Sold convertToEntity(SoldModel soldModel){
        Sold sold = new Sold();
        Warehouser warehouser = new Warehouser();
        warehouser.setId(soldModel.getWarehouserId());
        sold.setWarehouser(warehouser);
        sold.setId(soldModel.getId());
        sold.setSoldTime(LocalDateTime.now());
        sold.setProducts( soldModel.getProductsCountIds()
                .stream()
                .map((Integer t) -> new ProductsCount(t))
                .collect(Collectors.toList()));

        return sold;
    }
}
