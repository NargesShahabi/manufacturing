package com.example.manufacturing.status.manufactured.presentation;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
public class ManufacturedModel {

    private Integer id;
    private LocalDateTime manufacturedTime;
    private String producerName;
    private Integer producerCode;
    private Integer producerId;
    private List<Integer> productsCountIds;
}

