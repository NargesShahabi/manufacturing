package com.example.manufacturing.status.sold.service;

import com.example.manufacturing.status.sold.presentation.SoldModel;

import java.util.List;

public interface SoldService {

    List<SoldModel> index();

    SoldModel create(SoldModel soldModel);

    SoldModel update(SoldModel soldModel);

    String delete(Integer id);

    SoldModel show(Integer id);
}
