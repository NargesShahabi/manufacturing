package com.example.manufacturing.status.manufacturing.service;

import com.example.manufacturing.base.exceptions.EntityNotFoundException;
import com.example.manufacturing.base.exceptions.NotAllowedException;
import com.example.manufacturing.products.productioninfo.persistence.ProductsInfo;
import com.example.manufacturing.products.productioninfo.persistence.ProductsInfoRepository;
import com.example.manufacturing.products.productscount.persistence.ProductsCount;
import com.example.manufacturing.products.productscount.persistence.ProductsCountRepository;
import com.example.manufacturing.products.productscount.presentation.ProductsCountModel;
import com.example.manufacturing.products.productscount.service.ProductsCountService;
import com.example.manufacturing.status.manufacturing.persistence.Manufacturing;
import com.example.manufacturing.status.manufacturing.persistence.ManufacturingRepository;
import com.example.manufacturing.status.manufacturing.presentation.ManufacturingModel;
import com.example.manufacturing.status.manufacturing.presentation.ManufacturingProductsModel;
import com.example.manufacturing.user.producers.persistence.Producers;
import com.example.manufacturing.user.producers.persistence.ProducersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
@Qualifier("ManufacturingServiceImpl")
@Transactional(rollbackFor = Exception.class)
public class ManufacturingServiceImpl implements ManufacturingService {

    private ManufacturingRepository manufacturingRepository;
    private ProducersRepository producersRepository;
    private ProductsCountService productsCountService;
    private ProductsCountRepository productsCountRepository;
    private ProductsInfoRepository productsInfoRepository;


    @Autowired
    public ManufacturingServiceImpl(ManufacturingRepository manufacturingRepository,
                                    ProducersRepository producersRepository,
                                    ProductsCountService productsCountService,
                                    ProductsCountRepository productsCountRepository,
                                    ProductsInfoRepository productsInfoRepository) {
        this.manufacturingRepository = manufacturingRepository;
        this.producersRepository  = producersRepository;
        this.productsCountService  = productsCountService;
        this.productsCountRepository  = productsCountRepository;
        this.productsInfoRepository  = productsInfoRepository;
    }

    @Override
    public List<ManufacturingModel> index() {
        return manufacturingRepository.findAll().stream().map(value -> this.convertToModel(value)).collect(Collectors.toList());
    }

    @Override
    public ManufacturingModel create(ManufacturingModel manufacturingModel) {

        //Producer validation
        Producers producer = (Producers) producersRepository.findByProducerCode(manufacturingModel.getProducerCode())
                .orElseThrow(() -> new EntityNotFoundException("Producer with code "+ manufacturingModel.getProducerCode() +" not Found"));
        manufacturingModel.setProducerId(producer.getId());

        //Products validation
        ArrayList<Integer> wrongIds = new ArrayList<>();
        for(ManufacturingProductsModel item: manufacturingModel.getProductsType()){
            if(!productsInfoRepository.existsById(item.getProductId()))
                wrongIds.add(item.getProductId());
        }
        if(!wrongIds.isEmpty())
            throw new EntityNotFoundException("Products with ids "+ wrongIds +" not Found");


       Manufacturing manufacturing = manufacturingRepository.save(convertToEntity(manufacturingModel));

        //ProductsCount creation
        List<ProductsCountModel> productsCountModel = new ArrayList<>();
        manufacturingModel.getProductsType().
                forEach(value -> {productsCountModel.add(new ProductsCountModel(value.getProductId(), value.getNumber(), manufacturing.getId()));});
        productsCountService.create(productsCountModel);

        manufacturingModel.setId(manufacturing.getId());
        manufacturingModel.setStartTime(manufacturing.getStartTime());
        manufacturingModel.setProducerName(producer.getFName() + " " +producer.getLName());
        return manufacturingModel;
    }

    @Override
    public ManufacturingModel update(ManufacturingModel manufacturingModel) {
        if(!manufacturingRepository.existsById(manufacturingModel.getId()))
            throw new EntityNotFoundException("Manufacturing line with id "+ manufacturingModel.getId()+" not found");

        productsCountRepository.deleteAllByExisted(null);
        return (this.create(manufacturingModel));
    }

    @Override
    public String delete(Integer id) {
        try{
            manufacturingRepository.deleteById(id);
        }catch (Exception ex){
            ex.getMessage();
        }
        if(!manufacturingRepository.getById(id).getProductList().isEmpty())
            throw new NotAllowedException("Delete is not allowed");
        return "Successfully deleted";
    }

    @Override
    public ManufacturingModel show(Integer id) {
        Manufacturing manufacturing =  manufacturingRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Manufacturing line with id " + id  + " not found"));
        return convertToModel(manufacturing);
    }

    private ManufacturingModel convertToModel(Manufacturing manufacturing){
        ManufacturingModel manufacturingModel = new ManufacturingModel();
        manufacturingModel.setId(manufacturing.getId());
        manufacturingModel.setProducerCode(manufacturing.getProducer().getProducerCode());
        manufacturingModel.setProducerName(manufacturing.getProducer().getFName()+" "+manufacturing.getProducer().getLName());
        manufacturingModel.setProducerId(manufacturing.getProducer().getId());
        manufacturingModel.setStartTime(manufacturing.getStartTime());
        if (manufacturing.getProductList()!=null){
            List<ManufacturingProductsModel> MPMList = manufacturing.getProductList().stream()
                    .map(ManufacturingServiceImpl::convertToMPModel)
                    .collect(Collectors.toList());
            manufacturingModel.setProductsType(MPMList);
        }
        return manufacturingModel;
    }

    private static Manufacturing convertToEntity(ManufacturingModel manufacturingModel){
        Manufacturing manufacturing = new Manufacturing();
        Producers producer = new Producers();
        producer.setId(manufacturingModel.getProducerId());
        manufacturing.setId(manufacturingModel.getId());
        manufacturing.setProducer(producer);
        manufacturing.setProductList(
                manufacturingModel.getProductsType()
                        .stream()
                        .map(ManufacturingServiceImpl::convertToProductEntity)
                        .collect(Collectors.toList()));
        return manufacturing;
    }

    private static ProductsCount convertToProductEntity(ManufacturingProductsModel manufacturingProductsModel) {
        ProductsCount productsCount = new ProductsCount();
        ProductsInfo productInfo = new ProductsInfo();
        productInfo.setId(manufacturingProductsModel.getProductId());
        productsCount.setProductInfo(productInfo);
        productsCount.setNumber(manufacturingProductsModel.getNumber());
        return productsCount;

    }

    private static ManufacturingProductsModel convertToMPModel(ProductsCount productsCount) {
        ManufacturingProductsModel MPModel = new ManufacturingProductsModel();
        MPModel.setProductId(productsCount.getId());
        MPModel.setProductName(productsCount.getProductInfo().getName());
        MPModel.setNumber(productsCount.getNumber());
        return MPModel;

    }
}
