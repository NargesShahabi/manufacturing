package com.example.manufacturing.status.manufactured.service;

import lombok.Data;

@Data
public class ManufacturedProductModel {
    private Integer productId;
    private String productName;
    private Integer number;
}
