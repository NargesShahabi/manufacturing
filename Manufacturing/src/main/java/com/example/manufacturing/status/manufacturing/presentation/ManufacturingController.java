package com.example.manufacturing.status.manufacturing.presentation;

import com.example.manufacturing.status.manufacturing.service.ManufacturingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/manufacturing")
public class ManufacturingController {


    private ManufacturingService manufacturingService;

    @Autowired
    public ManufacturingController(ManufacturingService manufacturingService) {
        this.manufacturingService = manufacturingService;
    }

    @GetMapping({"/", ""})
    public List<ManufacturingModel> index() {
        return manufacturingService.index();
    }


    @PostMapping({"/", ""})
    public ManufacturingModel create(@RequestBody ManufacturingModel manufacturingModel) {
        return manufacturingService.create(manufacturingModel);
    }

    @PutMapping({"/", ""})
    public ManufacturingModel update(@RequestBody ManufacturingModel manufacturingModel) {
        return manufacturingService.update(manufacturingModel);
    }

    @DeleteMapping({"/{id}"})
    public String delete(@PathVariable Integer id){
        return manufacturingService.delete(id);
    }

    @GetMapping({"/{id}"})
    public ManufacturingModel show(@PathVariable Integer id){
        return manufacturingService.show(id);
    }

}
