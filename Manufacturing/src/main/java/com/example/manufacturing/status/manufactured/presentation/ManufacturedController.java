package com.example.manufacturing.status.manufactured.presentation;

import com.example.manufacturing.status.manufactured.service.ManufacturedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/manufactured")
public class ManufacturedController {


    private ManufacturedService manufacturedService;

    @Autowired
    public ManufacturedController(ManufacturedService manufacturedService) {
        this.manufacturedService = manufacturedService;
    }

    @GetMapping({"/", ""})
    public List<ManufacturedModel> index() {
        return manufacturedService.index();
    }


    @PostMapping({"/", ""})
    public ManufacturedModel create(@RequestBody ManufacturedModel manufacturedModel) {
        return manufacturedService.create(manufacturedModel);
    }

    @DeleteMapping({"/{id}"})
    public String delete(@PathVariable Integer id){
        return manufacturedService.delete(id);
    }

    @GetMapping({"/{id}"})
    public ManufacturedModel show(@PathVariable Integer id){
        return manufacturedService.show(id);
    }

}
