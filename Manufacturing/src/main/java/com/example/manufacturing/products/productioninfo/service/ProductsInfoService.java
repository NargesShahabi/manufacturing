package com.example.manufacturing.products.productioninfo.service;

import com.example.manufacturing.products.productioninfo.presentation.ProductsInfoModel;

import java.util.List;

public interface ProductsInfoService {

    List<ProductsInfoModel> index();

    ProductsInfoModel create(ProductsInfoModel ProductsInfoModel);

    ProductsInfoModel update(ProductsInfoModel ProductsInfoModel);

    ProductsInfoModel show(String name);
}
