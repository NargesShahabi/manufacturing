package com.example.manufacturing.products.productioninfo.service;

import com.example.manufacturing.base.exceptions.EntityNotFoundException;
import com.example.manufacturing.products.category.persistence.Category;
import com.example.manufacturing.products.category.persistence.CategoryRepository;
import com.example.manufacturing.products.category.presentation.CategoryModel;
import com.example.manufacturing.products.category.presentation.CategoryPInfoModel;
import com.example.manufacturing.products.category.service.CategoryServiceImpl;
import com.example.manufacturing.products.productioninfo.persistence.ProductsInfo;
import com.example.manufacturing.products.productioninfo.persistence.ProductsInfoRepository;
import com.example.manufacturing.products.productioninfo.presentation.ProductsInfoModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
@Qualifier("productsServiceImpl")
@Transactional(rollbackFor = Exception.class)
public class ProductsInfoServiceImpl implements ProductsInfoService {

    private ProductsInfoRepository productsInfoRepository;
    private CategoryServiceImpl categoryServiceImpl;
    private CategoryRepository categoryRepository;

    @Autowired
    public ProductsInfoServiceImpl(ProductsInfoRepository productsInfoRepository,
                                   CategoryServiceImpl categoryServiceImpl,
                                   CategoryRepository categoryRepository) {
        this.productsInfoRepository = productsInfoRepository;
        this.categoryServiceImpl = categoryServiceImpl;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<ProductsInfoModel> index() {
        return productsInfoRepository.findAll().stream().map(value -> this.convertToModel(value)).collect(Collectors.toList());
    }

    @Override
    public ProductsInfoModel create(ProductsInfoModel productsInfoModel) {
        //Category id validation
        if(!categoryRepository.existsById(productsInfoModel.getCategoryId()))
            throw new EntityNotFoundException("Category with id "+ productsInfoModel.getCategoryId()+" not found");
        ProductsInfo productsInfo = productsInfoRepository.save(this.convertToEntity(productsInfoModel));
        //Category update
        ArrayList<CategoryPInfoModel> PIML = new ArrayList<>();
        PIML.add(new CategoryPInfoModel(productsInfo.getId(),productsInfo.getName()));
        String categoryName = categoryRepository.getById(productsInfoModel.getCategoryId()).getName();
        categoryServiceImpl.update (new CategoryModel(productsInfoModel.getCategoryId(), categoryName, PIML));
        return convertToModel(productsInfo);
    }

    @Override
    public ProductsInfoModel update(ProductsInfoModel productsInfoModel) {
        //ProductsInfo id validation
        if(!productsInfoRepository.existsById(productsInfoModel.getId()))
            throw new EntityNotFoundException("Products Info with id "+ productsInfoModel.getId()+" not found");
        //Category id validation
        if(!categoryRepository.existsById(productsInfoModel.getCategoryId()))
            throw new EntityNotFoundException("Category with id "+ productsInfoModel.getCategoryId()+" not found");

        return convertToModel(productsInfoRepository.save(this.convertToEntity(productsInfoModel)));
    }

    @Override
    public ProductsInfoModel show(String name) {
        ProductsInfo productsInfo =  productsInfoRepository.findByName(name)
                .orElseThrow(() -> new EntityNotFoundException("ProductsInfo with id " + name  + " not found"));
        return convertToModel(productsInfo);
    }


    private ProductsInfoModel convertToModel(ProductsInfo productsInfo){
        ProductsInfoModel productsInfoModel = new ProductsInfoModel();
        productsInfoModel.setId(productsInfo.getId());
        productsInfoModel.setName(productsInfo.getName());
        productsInfoModel.setPrice(productsInfo.getPrice());
        productsInfoModel.setWeight(productsInfo.getWeight());
        productsInfoModel.setShelfLife(productsInfo.getShelfLife());
        productsInfoModel.setCategoryName(categoryRepository.getById(productsInfo.getCategory().getId()).getName());
        productsInfoModel.setCategoryId(productsInfo.getCategory().getId());
        return productsInfoModel;
    }

    private ProductsInfo convertToEntity(ProductsInfoModel productsInfoModel){
        ProductsInfo productsInfo = new ProductsInfo();
        Category category = new Category();
        category.setId(productsInfoModel.getCategoryId());
        productsInfo.setCategory(category);
        productsInfo.setId(productsInfoModel.getId());
        productsInfo.setName(productsInfoModel.getName());
        productsInfo.setShelfLife(productsInfoModel.getShelfLife());
        productsInfo.setWeight(productsInfoModel.getWeight());
        productsInfo.setPrice(productsInfoModel.getPrice());
        return productsInfo;
    }
}
