package com.example.manufacturing.products.category.service;

import com.example.manufacturing.products.category.presentation.CategoryModel;

import java.util.List;

public interface CategoryService {

    List<CategoryModel> index();

    CategoryModel create(CategoryModel CategoryModel);

    CategoryModel update(CategoryModel CategoryModel);

    String delete(Integer id);

    CategoryModel show(Integer id);
}
