package com.example.manufacturing.products.productscount.service;

import com.example.manufacturing.base.exceptions.EntityNotFoundException;
import com.example.manufacturing.products.allproducts.persistence.AllProducts;
import com.example.manufacturing.products.allproducts.presentation.AllProductsModel;
import com.example.manufacturing.products.productioninfo.persistence.ProductsInfo;
import com.example.manufacturing.products.productscount.persistence.ProductsCount;
import com.example.manufacturing.products.productscount.persistence.ProductsCountRepository;
import com.example.manufacturing.products.productscount.presentation.ProductsCountModel;
import com.example.manufacturing.status.manufactured.persistence.Manufactured;
import com.example.manufacturing.status.manufacturing.persistence.Manufacturing;
import com.example.manufacturing.status.sold.persistence.Sold;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


@Service
@Qualifier("ProductsCountServiceImpl")
@Transactional(rollbackFor = Exception.class)
public class ProductsCountServiceImpl implements ProductsCountService {

    private ProductsCountRepository productsCountRepository;

    @Autowired
    public ProductsCountServiceImpl(ProductsCountRepository productsCountRepository) {
        this.productsCountRepository = productsCountRepository;
    }

    @Override
    public List<ProductsCountModel> index() {
        return productsCountRepository.findAll().stream().map(value -> this.convertToModel(value)).collect(Collectors.toList());
    }

    @Override
    public List<ProductsCountModel> create(List<ProductsCountModel> productsCountModels){

        List<ProductsCount> PCM = productsCountRepository.saveAll(productsCountModels.stream()
                        .map(ProductsCountServiceImpl::convertToEntity)
                        .collect(Collectors.toList()));
        for(int i= 0; i< PCM.size();i++) {
            productsCountModels.get(i).setId(PCM.get(i).getId());
        }
        return productsCountModels;
    }

    @Override
    public List<ProductsCountModel> update(List<ProductsCountModel> productsCountModels) {

        List<ProductsCount> PCM = productsCountRepository.saveAll(productsCountModels.stream()
                        .map(ProductsCountServiceImpl::convertToEntity)
                        .collect(Collectors.toList()));
        for(int i= 0; i< PCM.size();i++) {
            productsCountModels.get(i).setId(PCM.get(i).getId());
        }
        return productsCountModels;
    }

    @Override
    public Boolean delete(Integer id) {
        try{
            productsCountRepository.deleteById(id);
        }catch (Exception e){
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @Override
    public ProductsCountModel show(Integer id) {
        ProductsCount productsCount =  productsCountRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("ProductsCount with id " + id + " Not Found"));
        return convertToModel(productsCount);
    }

    private static ProductsCount convertToEntity(ProductsCountModel productsCountModel) {
        ProductsCount productsCount = new ProductsCount();
        productsCount.setId(productsCountModel.getId());
        productsCount.setNumber(productsCountModel.getNumber());
        productsCount.setExisted(productsCountModel.getExisted());
        productsCount.setExpirationDate(productsCountModel.getExpirationDate());
        Manufacturing manufacturing = new Manufacturing();
        manufacturing.setId(productsCountModel.getManufacturingId());
        productsCount.setManufacturingLine(manufacturing);

        ProductsInfo productInfo = new ProductsInfo();
        productInfo.setId(productsCountModel.getProductInfoId());
        productsCount.setProductInfo(productInfo);

        if(productsCountModel.getManufacturedId()!=null) {
            Manufactured manufactured = new Manufactured();
            manufactured.setId(productsCountModel.getManufacturedId());
            productsCount.setManufactured(manufactured);
            productsCount.setAllProducts(
                    productsCountModel.getAllProductsModels()
                            .stream()
                            .map(ProductsCountServiceImpl::convertToPCEntity)
                            .collect(Collectors.toList()));
        }
        if (productsCountModel.getSoldId()!=null){
            Sold sold = new Sold();
            sold.setId(productsCountModel.getSoldId());
            productsCount.setSold(sold);
         }

         return productsCount;
    }

    private static AllProducts convertToPCEntity(AllProductsModel allProductsModel) {
        AllProducts allProducts = new AllProducts();
        allProducts.setId(allProducts.getId());
        allProducts.setProductCode(allProducts.getProductCode());
        return allProducts;
    }


    public static ProductsCountModel convertToModel(ProductsCount productsCount){
        ProductsCountModel productsCountModel = new ProductsCountModel();
        productsCountModel.setId(productsCount.getId());
        productsCountModel.setProductInfoName(productsCount.getProductInfo().getName());
        productsCountModel.setProductInfoId(productsCount.getProductInfo().getId());
        productsCountModel.setNumber(productsCount.getNumber());
        productsCountModel.setExisted(productsCount.getExisted());
        productsCountModel.setManufacturingId(productsCount.getManufacturingLine().getId());
        if(productsCount.getExisted()!=null){
            productsCountModel.setProductionDate(productsCount.getManufactured().getManufacturedTime());
            productsCountModel.setExpirationDate(productsCount.getExpirationDate());
            productsCountModel.setManufacturedId(productsCount.getManufactured().getId());
            if (productsCount.getAllProducts()!=null){
                List<AllPCountModel> APCList = productsCount.getAllProducts().stream()
                        .map(ProductsCountServiceImpl::convertToAllPCModel)
                        .collect(Collectors.toList());
            }
            if(!productsCount.getExisted())
                productsCountModel.setSoldId(productsCount.getSold().getId());
        }
        return productsCountModel;
    }

    private static AllPCountModel convertToAllPCModel(AllProducts allProducts) {
        AllPCountModel APCM = new AllPCountModel();
        APCM.setCode(allProducts.getProductCode());
        APCM.setId(allProducts.getId());
        return APCM;
    }

}
