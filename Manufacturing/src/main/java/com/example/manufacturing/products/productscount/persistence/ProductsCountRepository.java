package com.example.manufacturing.products.productscount.persistence;

import com.example.manufacturing.products.productioninfo.persistence.ProductsInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductsCountRepository extends JpaRepository<ProductsCount,Integer> {
    void deleteAllByExisted(Boolean e);
    Optional<ProductsCount> findById(Integer id);
}
