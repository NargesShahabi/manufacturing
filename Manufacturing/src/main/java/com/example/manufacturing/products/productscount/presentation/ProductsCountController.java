package com.example.manufacturing.products.productscount.presentation;

import com.example.manufacturing.products.productscount.service.ProductsCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/productsCount")
public class ProductsCountController {


    private ProductsCountService productsCountService;

    @Autowired
    public ProductsCountController(ProductsCountService productsCountService) {
        this.productsCountService = productsCountService;
    }

    @GetMapping({"/", ""})
    public List<ProductsCountModel> index() {
        return productsCountService.index();
    }


    @PutMapping({"/", ""})
    public List <ProductsCountModel> update(@RequestBody List <ProductsCountModel>  productsCountList) {
        return productsCountService.update(productsCountList);
    }

    @DeleteMapping({"/{id}"})
    public Boolean delete(@PathVariable Integer id){
        return productsCountService.delete(id);
    }

    @GetMapping({"/{id}"})
    public ProductsCountModel show(@PathVariable Integer id){
        return productsCountService.show(id);
    }

}
