package com.example.manufacturing.products.category.presentation;

import com.example.manufacturing.products.productioninfo.presentation.ProductsInfoModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryModel {
    private Integer id;
    private String name;
    private List<CategoryPInfoModel> categoryPInfoModel;

}
