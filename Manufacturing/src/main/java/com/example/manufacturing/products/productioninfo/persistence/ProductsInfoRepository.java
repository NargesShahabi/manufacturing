package com.example.manufacturing.products.productioninfo.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductsInfoRepository extends JpaRepository<ProductsInfo,Integer> {
    Optional<ProductsInfo> findByName(String name);
}
