package com.example.manufacturing.products.allproducts.persistence;
import com.example.manufacturing.base.config.RunConfig;
import com.example.manufacturing.products.productscount.persistence.ProductsCount;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "all_products", schema = RunConfig.db)
@Data
public class AllProducts {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "product_code", unique = true, nullable = false)
    private Integer productCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_count_id" , referencedColumnName = "id")
    private ProductsCount productsCount;

}
