package com.example.manufacturing.products.allproducts.service;

import com.example.manufacturing.products.allproducts.persistence.AllProducts;
import com.example.manufacturing.products.allproducts.presentation.AllProductsModel;

import java.util.List;

public interface AllProductsService {

    List<AllProductsModel> index();

    List <AllProductsModel> create(List <AllProductsModel> AllProductsModel);

    Boolean delete(Integer id);
}
