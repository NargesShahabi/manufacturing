package com.example.manufacturing.products.category.service;

import com.example.manufacturing.base.exceptions.EntityNotFoundException;
import com.example.manufacturing.base.exceptions.NotAllowedException;
import com.example.manufacturing.products.category.persistence.Category;
import com.example.manufacturing.products.category.persistence.CategoryRepository;
import com.example.manufacturing.products.category.presentation.CategoryModel;
import com.example.manufacturing.products.category.presentation.CategoryPInfoModel;
import com.example.manufacturing.products.productioninfo.persistence.ProductsInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


@Service
@Qualifier("productsServiceImpl")
@Transactional(rollbackFor = Exception.class)
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<CategoryModel> index() {
        return categoryRepository.findAll().stream()
                .map(value -> this.convertToModel(value)).collect(Collectors.toList());
    }

    @Override
    public CategoryModel create(CategoryModel categoryModel) {
        Category category = categoryRepository.save(this.convertToEntity(categoryModel));
        categoryModel.setId(category.getId());
        return categoryModel;
    }

    @Override
    public CategoryModel update(CategoryModel categoryModel) {
        //Category id validation
        if(!categoryRepository.existsById(categoryModel.getId()))
            throw new EntityNotFoundException("Category with id "+ categoryModel.getId()+" not found");

        categoryRepository.save(this.convertToEntity(categoryModel));
        return categoryModel;
    }

    @Override
    public String delete(Integer id) {
        try{
            categoryRepository.deleteById(id);
        }catch (Exception ex){
            ex.getMessage();
        }

        if(!categoryRepository.getById(id).getProductsInfoCollection().isEmpty())
            throw new NotAllowedException("Delete is not allowed");

        return "Successfully deleted";
    }

    @Override
    public CategoryModel show(Integer id) {
        Category category =  categoryRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Category with id " + id  + " not found"));
        return convertToModel(category);    }


    private CategoryModel convertToModel(Category category) {
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setId(category.getId());
        categoryModel.setName(category.getName());
        if (category.getProductsInfoCollection() != null) {
            List<CategoryPInfoModel> categoryPInfoModel =
                    category.getProductsInfoCollection().stream()
                            .map(CategoryServiceImpl::convertToCategoryPInfoModel).collect(Collectors.toList());
            categoryModel.setCategoryPInfoModel(categoryPInfoModel);
        }
        return categoryModel;
    }

    private Category convertToEntity(CategoryModel categoryModel) {
        Category category = new Category();
        category.setName(categoryModel.getName());
        category.setId(categoryModel.getId());
        if(categoryModel.getCategoryPInfoModel()!=null){
        category.setProductsInfoCollection(
                categoryModel.getCategoryPInfoModel()
                        .stream()
                        .map(CategoryServiceImpl::convertToCategoryPInfoEntity)
                        .collect(Collectors.toList()));}
        return category;
    }

    private static ProductsInfo convertToCategoryPInfoEntity(CategoryPInfoModel categoryPInfoModel) {
        ProductsInfo productInfo = new ProductsInfo();
        productInfo.setId(categoryPInfoModel.getProductsInfoId());
        productInfo.setName(categoryPInfoModel.getProductsInfoName());
        return productInfo;

    }

    private static CategoryPInfoModel convertToCategoryPInfoModel(ProductsInfo productsInfo) {
        CategoryPInfoModel categoryPInfoModel = new CategoryPInfoModel();
        categoryPInfoModel.setProductsInfoId(productsInfo.getId());
        categoryPInfoModel.setProductsInfoName(productsInfo.getName());
        return categoryPInfoModel;
    }
}
