package com.example.manufacturing.products.productscount.service;

import lombok.Data;

@Data
public class AllPCountModel {
    private Integer id;
    private Integer code;
}
