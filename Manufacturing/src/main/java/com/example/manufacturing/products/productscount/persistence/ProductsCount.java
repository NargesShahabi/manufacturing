package com.example.manufacturing.products.productscount.persistence;
import com.example.manufacturing.base.config.RunConfig;
import com.example.manufacturing.products.allproducts.persistence.AllProducts;
import com.example.manufacturing.products.productioninfo.persistence.ProductsInfo;
import com.example.manufacturing.status.manufactured.persistence.Manufactured;
import com.example.manufacturing.status.manufacturing.persistence.Manufacturing;
import com.example.manufacturing.status.sold.persistence.Sold;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;

@Entity
@Table(name = "products_count", schema = RunConfig.db)
@NoArgsConstructor
@Data
public class ProductsCount {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "number", nullable = false)
    private Integer number;

    @Column(name = "expiration_date")
    LocalDateTime expirationDate;

    @Column (name = "status")
    Boolean existed;

    @OneToMany(mappedBy = "productsCount", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Collection <AllProducts> allProducts;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Products_info_id" , referencedColumnName = "id")
    private ProductsInfo productInfo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "manufacturing_line_id" , referencedColumnName = "id")
    private Manufacturing manufacturingLine;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "manufactured_id" , referencedColumnName = "id")
    private Manufactured manufactured;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sold_id" , referencedColumnName = "id")
    private Sold sold;

    public ProductsCount(Integer id) {
        this.id = id;
    }
}
