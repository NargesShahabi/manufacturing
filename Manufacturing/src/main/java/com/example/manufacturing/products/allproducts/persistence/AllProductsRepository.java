package com.example.manufacturing.products.allproducts.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AllProductsRepository extends JpaRepository<AllProducts,Integer> {
}
