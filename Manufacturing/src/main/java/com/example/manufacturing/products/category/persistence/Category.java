package com.example.manufacturing.products.category.persistence;


import com.example.manufacturing.base.config.RunConfig;
import com.example.manufacturing.products.productioninfo.persistence.ProductsInfo;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "category", schema = RunConfig.db)
@Data
public class Category {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
    private Collection<ProductsInfo> productsInfoCollection;
}
