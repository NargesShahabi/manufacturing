package com.example.manufacturing.products.allproducts.presentation;

import com.example.manufacturing.products.allproducts.service.AllProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/allProducts")
public class AllProductsController {


    private AllProductsService allProductsService;

    @Autowired
    public AllProductsController(AllProductsService allProductsService) {
        this.allProductsService = allProductsService;
    }

    @GetMapping({"/", ""})
    public List<AllProductsModel> index() {
        return allProductsService.index();
    }

    @DeleteMapping({"/{id}"})
    public Boolean delete(@PathVariable Integer id){
        return allProductsService.delete(id);
    }

}
