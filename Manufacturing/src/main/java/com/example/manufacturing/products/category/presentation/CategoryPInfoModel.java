package com.example.manufacturing.products.category.presentation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryPInfoModel {
    private Integer productsInfoId;
    private String productsInfoName;
}
