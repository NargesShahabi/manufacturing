package com.example.manufacturing.products.allproducts.presentation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AllProductsModel {

    private Integer id;
    private Integer code;
    private Integer productCountId;
    private String productName;
    private LocalDateTime productionDate;
    private LocalDateTime expirationDate;
}
