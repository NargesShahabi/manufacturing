package com.example.manufacturing.products.productioninfo.presentation;

import com.example.manufacturing.products.productioninfo.service.ProductsInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/productsInfo")
public class ProductsInfoController {


    private ProductsInfoService productsInfoService;

    @Autowired
    public ProductsInfoController(ProductsInfoService productsInfoService) {
        this.productsInfoService = productsInfoService;
    }

    @GetMapping({"/", ""})
    public List<ProductsInfoModel> index() {
        return productsInfoService.index();
    }


    @PostMapping({"/", ""})
    public ProductsInfoModel create(@RequestBody ProductsInfoModel productsInfoModel) {
        return productsInfoService.create(productsInfoModel);
    }

    @PutMapping({"/", ""})
    public ProductsInfoModel update(@RequestBody ProductsInfoModel productsInfoModel) {
        return productsInfoService.update(productsInfoModel);
    }

    @GetMapping({"/{name}"})
    public ProductsInfoModel show(@PathVariable String name){
        return productsInfoService.show(name);
    }

}
