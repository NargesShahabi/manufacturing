package com.example.manufacturing.products.productscount.presentation;

import lombok.Data;

@Data
public class AllPCModel {

    private Integer allPCId;
    private Integer allPCCode;
}
