package com.example.manufacturing.products.productscount.presentation;

import com.example.manufacturing.products.allproducts.presentation.AllProductsModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductsCountModel {

    private Integer id;
    private String productInfoName;
    private Integer productInfoId;
    private Integer number;
    private Boolean existed;
    private LocalDateTime productionDate;
    private LocalDateTime expirationDate;
    private Integer manufacturingId;
    private Integer manufacturedId;
    private Integer soldId;
    private List<AllProductsModel> allProductsModels= new ArrayList<>();

    public ProductsCountModel(Integer productInfoId, Integer number, Integer manufacturingId) {
        this.productInfoId = productInfoId;
        this.number = number;
        this.manufacturingId = manufacturingId;

    }
}
