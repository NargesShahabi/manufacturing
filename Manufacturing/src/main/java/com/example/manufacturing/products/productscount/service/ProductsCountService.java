package com.example.manufacturing.products.productscount.service;

import com.example.manufacturing.products.productscount.presentation.ProductsCountModel;

import java.util.List;

public interface ProductsCountService {

    List<ProductsCountModel> index();

    List <ProductsCountModel> create(List <ProductsCountModel> productsCountModels);

    List <ProductsCountModel> update(List<ProductsCountModel> productsCountList);

    Boolean delete(Integer id);

    ProductsCountModel show(Integer id);
}
