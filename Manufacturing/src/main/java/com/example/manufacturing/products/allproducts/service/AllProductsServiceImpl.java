package com.example.manufacturing.products.allproducts.service;

import com.example.manufacturing.products.allproducts.persistence.AllProducts;
import com.example.manufacturing.products.allproducts.persistence.AllProductsRepository;
import com.example.manufacturing.products.allproducts.presentation.AllProductsModel;
import com.example.manufacturing.products.productscount.persistence.ProductsCount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


@Service
@Qualifier("productsServiceImpl")
@Transactional(rollbackFor = Exception.class)
public class AllProductsServiceImpl implements AllProductsService {

    private AllProductsRepository allProductsRepository;

    @Autowired
    public AllProductsServiceImpl(AllProductsRepository allProductsRepository) {
        this.allProductsRepository = allProductsRepository;
    }

    @Override
    public List<AllProductsModel> index() {
        return allProductsRepository.findAll().stream().map(value -> this.convertToModel(value)).collect(Collectors.toList());
    }

    @Override
    public List<AllProductsModel> create(List<AllProductsModel> allProductsModel) {
      List <AllProducts> all = allProductsRepository.saveAll(
                allProductsModel.stream()
                        .map(AllProductsServiceImpl::convertToEntity)
                        .collect(Collectors.toList()));
        for(int i= 0; i< all.size();i++) {
            allProductsModel.get(i).setId(all.get(i).getId());
        }
       return allProductsModel;
    }

    @Override
    public Boolean delete(Integer id) {
        try{
            allProductsRepository.deleteById(id);
        }catch (Exception e){
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    private AllProductsModel convertToModel(AllProducts allProducts){
        AllProductsModel allProductsModel = new AllProductsModel();
        ProductsCount PC = allProducts.getProductsCount();
        allProductsModel.setId(allProducts.getId());
        allProductsModel.setCode(allProducts.getProductCode());
        allProductsModel.setProductName(PC.getProductInfo().getName());
        allProductsModel.setProductionDate(PC.getManufactured().getManufacturedTime());
        allProductsModel.setExpirationDate(PC.getExpirationDate());
        return allProductsModel;
    }

    private static AllProducts convertToEntity(AllProductsModel allProductsModel){
        AllProducts allProducts = new AllProducts();
        ProductsCount productsCount = new ProductsCount();
        productsCount.setId(allProductsModel.getProductCountId());
        allProducts.setProductsCount(productsCount);
        allProducts.setId(allProductsModel.getId());
        allProducts.setProductCode(allProductsModel.getCode());
        return allProducts;
    }
}
