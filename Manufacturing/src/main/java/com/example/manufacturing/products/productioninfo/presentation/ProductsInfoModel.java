package com.example.manufacturing.products.productioninfo.presentation;

import lombok.Data;

@Data
public class ProductsInfoModel {
    private Integer id;
    private String name;
    private Integer shelfLife;
    private Integer weight;
    private Integer price;
    private Integer categoryId;
    private String categoryName;
}
