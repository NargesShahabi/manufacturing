package com.example.manufacturing.products.category.presentation;

import com.example.manufacturing.products.category.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/category")
public class CategoryController {


    private CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping({"/", ""})
    public List<CategoryModel> index() {
        return categoryService.index();
    }


    @PostMapping({"/", ""})
    public CategoryModel create(@RequestBody CategoryModel categoryModel) {
        return categoryService.create(categoryModel);
    }

    @PutMapping({"/", ""})
    public CategoryModel update(@RequestBody CategoryModel categoryModel) {
        return categoryService.update(categoryModel);
    }

    @DeleteMapping({"/{id}"})
    public String delete(@PathVariable Integer id){
        return categoryService.delete(id);
    }

    @GetMapping({"/{id}"})
    public CategoryModel show(@PathVariable Integer id){
        return categoryService.show(id);
    }

}
