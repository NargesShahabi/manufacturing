package com.example.manufacturing.products.productioninfo.persistence;


import com.example.manufacturing.base.config.RunConfig;
import com.example.manufacturing.products.category.persistence.Category;
import com.example.manufacturing.products.productscount.persistence.ProductsCount;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "products_info", schema = RunConfig.db)
@Data
public class ProductsInfo {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "shelf_life")
    private Integer shelfLife;

    @Column(name = "weight")
    private Integer weight;

    @Column(name = "price")
    private Integer price;

    @OneToMany(mappedBy = "productInfo", fetch = FetchType.LAZY)
    private Collection <ProductsCount> productsCount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private Category category;

}
