package com.example.manufacturing.base.exceptions;

import lombok.Data;

@Data
public class EntityNotFoundException extends RuntimeException {
    private  String message;

    public EntityNotFoundException(String message){
        this.message = message;
    }
}
