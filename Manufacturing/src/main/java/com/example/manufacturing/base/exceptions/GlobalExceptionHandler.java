package com.example.manufacturing.base.exceptions;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Object> handleEntityNotFoundException(Exception ex){

        ApiError apiError = new ApiError (HttpStatus.NOT_FOUND, LocalDateTime.now(),ex.getMessage());
              return new ResponseEntity<>(apiError,apiError.getStatus());
    }
    @ExceptionHandler(NotAllowedException.class)
    public ResponseEntity<Object> handleNotAllowedExceptionException(Exception ex){

        ApiError apiError = new ApiError (HttpStatus.METHOD_NOT_ALLOWED, LocalDateTime.now(),ex.getMessage());
        return new ResponseEntity<>(apiError,apiError.getStatus());
    }
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(Exception ex){
        ApiError apiError = new ApiError (HttpStatus.INTERNAL_SERVER_ERROR, LocalDateTime.now(),ex.getMessage());
        return new ResponseEntity<>(apiError,apiError.getStatus());
    }
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintViolationException(Exception ex){
        ApiError apiError = new ApiError (HttpStatus.INTERNAL_SERVER_ERROR, LocalDateTime.now(), ex.getMessage());
        return new ResponseEntity<>(apiError,apiError.getStatus());
    }
}
