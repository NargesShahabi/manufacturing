package com.example.manufacturing.base.exceptions;

import lombok.Data;

@Data
public class NotAllowedException extends RuntimeException {
    private  String message;

    public NotAllowedException(String message){
        this.message = message;
    }
}
