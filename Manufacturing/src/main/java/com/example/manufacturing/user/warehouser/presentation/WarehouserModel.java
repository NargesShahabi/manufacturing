package com.example.manufacturing.user.warehouser.presentation;

import com.example.manufacturing.user.personnel.PersonnelModel;
import lombok.Data;

@Data
public class WarehouserModel extends PersonnelModel {

    private Integer id;
    private Integer warehouserCode;
}
