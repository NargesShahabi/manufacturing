package com.example.manufacturing.user.producers.presentation;

import com.example.manufacturing.user.personnel.PersonnelModel;
import lombok.Data;

@Data
public class ProducersModel extends PersonnelModel {
    private Integer id;
    private Integer producerCode;
}
