package com.example.manufacturing.user.warehouser.persistence;


import com.example.manufacturing.base.config.RunConfig;
import com.example.manufacturing.status.sold.persistence.Sold;
import com.example.manufacturing.user.personnel.Personnel;
import lombok.Data;
import javax.persistence.*;
import java.util.Collection;


@Entity
@Table(name = "warehouser", schema = RunConfig.db)
@Data
public class Warehouser extends Personnel {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "warehouser_code", unique = true, nullable = false, length = 8)
    private Integer warehouserCode;

    @OneToMany(mappedBy = "warehouser", fetch = FetchType.LAZY)
    private Collection<Sold> sold;

}
