package com.example.manufacturing.user.producers.service;

import com.example.manufacturing.user.producers.presentation.ProducersModel;

import java.util.List;

public interface ProducersService {

    List<ProducersModel> index();

    ProducersModel create(ProducersModel producersModel);

    ProducersModel update(ProducersModel producersModel);

    ProducersModel show(Integer id);
}
