package com.example.manufacturing.user.producers.service;

import com.example.manufacturing.base.exceptions.EntityNotFoundException;
import com.example.manufacturing.user.producers.persistence.Producers;
import com.example.manufacturing.user.producers.persistence.ProducersRepository;
import com.example.manufacturing.user.producers.presentation.ProducersModel;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


@Service
@Qualifier("ProducersServiceImpl")
@Transactional(rollbackFor = Exception.class)
public class ProducersServiceImpl implements ProducersService {

    private ProducersRepository producersRepository;

    @Autowired
    public ProducersServiceImpl(ProducersRepository producersRepository) {
        this.producersRepository = producersRepository;
    }

    @Override
    public List <ProducersModel> index() {
        return producersRepository.findAll().stream()
                .map(value -> this.convertToModel(value))
                .collect(Collectors.toList());
    }

    @Override
    public ProducersModel create(ProducersModel producersModel) {
        Producers producers = producersRepository.save(this.convertToEntity(producersModel));
        producersModel.setId(producers.getId());
        return producersModel;
    }

    @Override
    public ProducersModel update(ProducersModel producersModel) {
        // Producer id validation
        if(!producersRepository.existsById(producersModel.getId()))
            throw new EntityNotFoundException("Producers with id "+ producersModel.getId()+" not found");

        producersRepository.save(this.convertToEntity(producersModel));
        return producersModel;
    }

    @Override
    public ProducersModel show(Integer id) {
        Producers producer =  producersRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Producer with id " + id  + " not found"));
        return convertToModel(producer);
    }

    private ProducersModel convertToModel(Producers producers){
        ProducersModel producersModel = new ProducersModel();
        producersModel.setId(producers.getId());
        producersModel.setFName(producers.getFName());
        producersModel.setLName(producers.getLName());
        producersModel.setNationalCode(producers.getNationalCode());
        producersModel.setTell(producers.getTell());
        producersModel.setProducerCode(producers.getProducerCode());
        return producersModel;
    }

    private Producers convertToEntity(ProducersModel producersModel){
        Producers producers = new Producers();
        producers.setId(producersModel.getId());
        producers.setFName(producersModel.getFName());
        producers.setLName(producersModel.getLName());
        try {
        producers.setNationalCode(producersModel.getNationalCode());
        producers.setTell(producersModel.getTell());
        producers.setProducerCode(producersModel.getProducerCode());
        }catch (ConstraintViolationException ex){}
        return producers;
    }
}
