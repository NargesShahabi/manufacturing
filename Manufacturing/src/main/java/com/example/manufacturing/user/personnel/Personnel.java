package com.example.manufacturing.user.personnel;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Data
@Table(name = "manufacturing")
@MappedSuperclass
public class Personnel {

    @Column(name = "first_name")
    private String fName;

    @Column(name = "last_name")
    private String lName;

    @Column(name = "national_code", length = 10)
    private String nationalCode;

    @Column(name = "tell", length = 12)
    private String tell;
}
