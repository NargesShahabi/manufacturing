package com.example.manufacturing.user.warehouser.service;

import com.example.manufacturing.user.warehouser.presentation.WarehouserModel;

import java.util.List;

public interface WarehouserService {

    List<WarehouserModel> index();

    WarehouserModel create(WarehouserModel warehouseModel);

    WarehouserModel update(WarehouserModel warehouseModel);

    WarehouserModel show(Integer id);
}
