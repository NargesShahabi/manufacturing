package com.example.manufacturing.user.warehouser.service;

import com.example.manufacturing.base.exceptions.EntityNotFoundException;
import com.example.manufacturing.base.exceptions.NotAllowedException;
import com.example.manufacturing.user.producers.persistence.Producers;
import com.example.manufacturing.user.warehouser.persistence.Warehouser;
import com.example.manufacturing.user.warehouser.persistence.WarehouserRepository;
import com.example.manufacturing.user.warehouser.presentation.WarehouserModel;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


@Service
@Qualifier("WarehouserServiceImpl")
@Transactional(rollbackFor = Exception.class)
public class WarehouserServiceImpl implements WarehouserService {

    private WarehouserRepository warehouserRepository;

    @Autowired
    public WarehouserServiceImpl(WarehouserRepository warehouseRepository) {
        this.warehouserRepository = warehouseRepository;
    }

    @Override
    public List<WarehouserModel> index() {
        return warehouserRepository.findAll().stream()
                .map(value -> this.convertToModel(value))
                .collect(Collectors.toList());
    }

    @Override
    public WarehouserModel create(WarehouserModel warehouserModel) {
        Warehouser warehouser = warehouserRepository.save(this.convertToEntity(warehouserModel));
        warehouserModel.setId(warehouser.getId());
        return warehouserModel;
    }

    @Override
    public WarehouserModel update(WarehouserModel warehouserModel) {
        // Warehouser id validation
        if(!warehouserRepository.existsById(warehouserModel.getId()))
            throw new EntityNotFoundException("Warehouser with id "+ warehouserModel.getId()+" not found");

        warehouserRepository.save(this.convertToEntity(warehouserModel));
        return warehouserModel;
    }

    @Override
    public WarehouserModel show(Integer id) {
        Warehouser warehouser =  warehouserRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Warehouser with id " + id  + " not found"));
        return convertToModel(warehouser);
    }

    private WarehouserModel convertToModel(Warehouser warehouser){
        WarehouserModel warehouserModel = new WarehouserModel();
        warehouserModel.setId(warehouser.getId());
        warehouserModel.setFName(warehouser.getFName());
        warehouserModel.setLName(warehouser.getLName());
        warehouserModel.setNationalCode(warehouser.getNationalCode());
        warehouserModel.setTell(warehouser.getTell());
        warehouserModel.setWarehouserCode(warehouser.getWarehouserCode());
        return warehouserModel;
    }

    private Warehouser convertToEntity(WarehouserModel warehouserModel){
        Warehouser warehouser = new Warehouser();
        warehouser.setId(warehouserModel.getId());
        warehouser.setFName(warehouserModel.getFName());
        warehouser.setLName(warehouserModel.getLName());
        try{
        warehouser.setNationalCode(warehouserModel.getNationalCode());
        warehouser.setTell(warehouserModel.getTell());
        warehouser.setWarehouserCode(warehouserModel.getWarehouserCode());
       }catch (
       ConstraintViolationException ex){}
        return warehouser;
    }
}
