package com.example.manufacturing.user.producers.presentation;

import com.example.manufacturing.user.producers.service.ProducersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/producers")
public class ProducersController{


    private ProducersService producersService;

    @Autowired
    public ProducersController(ProducersService producersService) {
        this.producersService = producersService;
    }

    @GetMapping({"/", ""})
    public List<ProducersModel> index() {
        return producersService.index();
    }


    @PostMapping({"/", ""})
    public ProducersModel create(@RequestBody ProducersModel producersModel) {
        return producersService.create(producersModel);
    }

    @PutMapping({"/", ""})
    public ProducersModel update(@RequestBody ProducersModel producersModel) {
        return producersService.update(producersModel);
    }

    @GetMapping("/{id}")
    public  ProducersModel findEmployeeById(@PathVariable Integer id) {
        return producersService.show(id);
    }



}
