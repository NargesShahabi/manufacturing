package com.example.manufacturing.user.warehouser.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WarehouserRepository extends JpaRepository<Warehouser,Integer> {
    Optional<Object>  findByWarehouserCode(Integer warehouserCode);
}
