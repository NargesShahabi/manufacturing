package com.example.manufacturing.user.producers.persistence;


import com.example.manufacturing.base.config.RunConfig;
import com.example.manufacturing.status.manufactured.persistence.Manufactured;
import com.example.manufacturing.status.manufacturing.persistence.Manufacturing;
import com.example.manufacturing.user.personnel.Personnel;
import lombok.Data;
import javax.persistence.*;
import java.util.Collection;


@Entity
@Table(name = "producer", schema = RunConfig.db)
@Data
public class Producers extends Personnel {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Integer id;

    @Column(name = "producer_code", unique = true, nullable = false, length = 8)
    private Integer producerCode;

    @OneToMany(mappedBy = "producer",fetch = FetchType.LAZY)
    private Collection <Manufacturing> manufacturingList;

    @OneToMany(mappedBy = "producer",fetch = FetchType.LAZY)
    private Collection <Manufactured> manufacturedList;
}
