package com.example.manufacturing.user.personnel;

import lombok.Data;

import java.sql.Date;

@Data
public class PersonnelModel {

    private String fName;
    private String lName;
    private String nationalCode;
    private String tell;

}
