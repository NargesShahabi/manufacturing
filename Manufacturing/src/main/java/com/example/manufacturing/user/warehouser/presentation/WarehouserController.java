package com.example.manufacturing.user.warehouser.presentation;

import com.example.manufacturing.user.warehouser.service.WarehouserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/warehouser")
public class WarehouserController {


    private WarehouserService warehouserService;

    @Autowired
    public WarehouserController(WarehouserService warehouserService) {
        this.warehouserService = warehouserService;
    }

    @GetMapping({"/", ""})
    public List<WarehouserModel> index() {
        return warehouserService.index();
    }


    @PostMapping({"/", ""})
    public WarehouserModel create(@RequestBody WarehouserModel warehouseModel) {
        return warehouserService.create(warehouseModel);
    }

    @PutMapping({"/", ""})
    public WarehouserModel update(@RequestBody WarehouserModel warehouserModel) {
        return warehouserService.update(warehouserModel);
    }

    @GetMapping({"/{id}"})
    public WarehouserModel show(@PathVariable Integer id){
        return warehouserService.show(id);
    }
}
