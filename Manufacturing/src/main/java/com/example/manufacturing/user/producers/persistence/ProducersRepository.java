package com.example.manufacturing.user.producers.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProducersRepository extends JpaRepository<Producers,Integer> {
       Optional<Object> findByProducerCode(Integer Code);
}
